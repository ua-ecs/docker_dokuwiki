#!/usr/bin/env bash

# Signal handling for entrypoint script
# Ref: https://medium.com/@gchudnov/trapping-signals-in-docker-containers-7a57fdda7d86

#set -x

pid=0

# SIGUSR1-handler
usr1_handler() {
  echo "Received SIGUSR1"
  echo "Reloading Apache pid $pid"
  /usr/sbin/httpd -k graceful
  #pid="$!"
  #echo "Reloaded apache to pid $pid"
}

# SIGTERM-handler
term_handler() {
  if [ $pid -ne 0 ]; then
    echo "Received SIGTERM, exiting"
    #kill -SIGTERM "$pid"
    /usr/sbin/httpd -k graceful-stop
    wait "$pid"
  fi
  exit 143; # 128 + 15 -- SIGTERM
}

# setup handlers
# on callback, kill the last background process, which is `tail -f /dev/null` and execute the specified handler
#trap 'kill ${!}; usr1_handler' SIGUSR1
#trap 'kill ${!}; term_handler' SIGTERM
trap 'usr1_handler' SIGUSR1
trap 'term_handler' SIGTERM

# Start rsyslog
/usr/sbin/rsyslogd -i /var/run/syslogd.pid -f /etc/rsyslog.conf -n &

# Connect to the NFS export with the dokuwiki content
mkdir /nfsdokuwiki
mount $NFSMOUNT /nfsdokuwiki

# Call update process, in case there's an update in the Dokuwiki version
if [ -z $1 ] ; then
  # A startup value was passed in
  UPDATE='run'
else
  UPDATE=$1
fi

/root/update.sh $UPDATE
RC=$?
if [ $RC -gt 0 ] ; then
  echo "Update check failed with returncode $RC"
  exit
fi

# Bind mount all the important bits back to the dokuwiki base folder
mount -o bind /nfsdokuwiki/data /dokuwiki/data
mount -o bind /nfsdokuwiki/lib/plugins /dokuwiki/lib/plugins
mount -o bind /nfsdokuwiki/conf /dokuwiki/conf
mount -o bind /nfsdokuwiki/lib/tpl /dokuwiki/lib/tpl
mount -o bind /nfsdokuwiki/var/log/httpd /var/log/httpd

# Set the %SITEADMIN and %SITENAME variables in the http config
if [ -n "${SITE_ADMIN}" ] && [ -n "${SITE_FQDN}" ] ; then
    sed -i -e "s/_SITEADMIN/${SITE_ADMIN}/g" -e "s/_SITENAME/${SITE_FQDN}/g" /etc/httpd/vhosts.d/docs.conf
else
    echo 'Missing variables $SITE_FQDN or $SITE_ADMIN'
fi

# Export root's environment variables for later use by cron
/bin/printenv | sed 's/^\([a-zA-Z0-9_]*\)=\(.*\)$/export \1="\2"/g' > /root/.env

# Start crond. Delayed so it doesn't stomp on the mounts.
# Conditional start if S3Bucket is defined
if [ -n "${S3Bucket}" ] ; then
    /usr/sbin/crond -s -n &
fi

# Start apache
/usr/sbin/httpd -DFOREGROUND &
pid="$!"
echo "Running apache on pid $pid"

# Do something to keep the container live
while [ 1 ]
do
  sleep 1
  echo "waiting indefinitely on pid $pid"
  wait "$pid"
  #wait $!
done
