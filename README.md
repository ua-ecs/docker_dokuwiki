DokuWiki docker container
=========================


To run image:
-------------

	docker run \
		-p 80:80 \
		-e NFSMOUNT="fs-0123abcd.efs.us-west-2.amazonaws.com:/" \
		-e S3Bucket="docs-ENV.uits.arizona.edu-archive" \
		-e SITE_ADMIN="uits-unix-team@list.arizona.edu" \
		-e SITE_FQDN="docs-ENV.uits.arizona.edu" \
		--name my_wiki dokuwiki_uits:YYYYMMDD

You can now visit the install page to configure your new DokuWiki wiki.

For example, if you are running container locally, you can acces the page 
in browser by going to http://127.0.0.1/install.php

**NOTE:** this variation of the DokuWiki docker container is intended for deployment under ECS in AWS, and generally requires having access to an EFS NFS export for content and configuration storage as well as to an S3 bucket for backups.

Run a specific version
----------------------

When running the container you can specify version to download from docker registry by using tags like **YYYYMMDD** which contains date-specific release. See the contents of the container repository for older versions available.


To update the image
-------------------

Where the original version of this docker container used docker volumes for persisting data and configuration between deployments and updates to the software, this variation uses EFS to store that persistent data. Because of that, the docker container can be updated/rebuilt without any special steps being needed to ensure the content remains available across the update. This ***should*** include updates to the version of DokuWiki installed in the container.

Optimizing your wiki
--------------------

Instead of Lighttpd as used in the original version, this variation uses Apache with PHP. The Apache configuration has been set with AWS ECS deployment fronted by ALB (ELBv2) load balancer, with the SSL cert terminated at the load balancer. Included in that is URL rewriting for HTTPS redirect and customized logging to show the X-Forwarded-For IP address of the clients (as passed by the ALB).

Build your own
--------------

	docker build -t my_dokuwiki .

Deployment into AWS
-------------------

See: https://confluence.arizona.edu/display/ECS/Dokuwiki+CloudFormation+Deployment+Guide

