#!/usr/bin/env bash
sed -i '/<IfModule log_config_module>/,/<\/IfModule>/ {
        :loop
        s/"" combined/" %D" combined/
        s/^\(\s\+\)\(CustomLog.*\)$/\1#\2/
        /^\s\+#CustomLog.*combined$/a\
    CustomLog "logs\/access_log" combined env=!forwarded\
    CustomLog "logs\/access_log" proxy env=forwarded
        /b" common/a\
    LogFormat "%{X-Forwarded-For}i %l %u %t \\"%r\\" %>s %b \\"%{Referer}i\\" \\"%{User-Agent}i\\" %D %{X-Forwarded-Proto}i" proxy\
    SetEnvIf X-Forwarded-For "^\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}.*" forwarded
        n
        b loop
        }' /etc/httpd/conf/httpd.conf

