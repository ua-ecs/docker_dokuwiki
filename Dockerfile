# VERSION 0.1
# AUTHOR:         Miroslav Prasil <miroslav@prasil.info>
# DESCRIPTION:    Image with DokuWiki & lighttpd
# TO_BUILD:       docker build -t mprasil/dokuwiki .
# TO_RUN:         docker run -d -p 80:80 --name my_wiki mprasil/dokuwiki

# Modified for use by UofA UITS docs
# Woody Alhaddad <dalhaddad@email.arizona.edu>
# 2017/05/19

FROM centos:7
# MAINTAINER Miroslav Prasil <miroslav@prasil.info>
MAINTAINER Woody Alhaddad <dalhaddad@email.arizona.edu>

# Set the version you want of Twiki
# See:
#
#     https://download.dokuwiki.org/
#
# for the Stable release version, then get the matching
# direct download link and checksum from:
#
#     https://download.dokuwiki.org/archive
#
ENV DOKUWIKI_VERSION 2018-04-22a
ENV DOKUWIKI_CSUM 18765a29508f96f9882349a304bffc03

ENV LAST_REFRESHED 04 Sept 2018

# Update & install packages & cleanup afterwards
RUN \
    yum -y install epel-release && \
    yum clean all && yum makecache && yum -y update && \
    rpm --import http://rpms.remirepo.net/RPM-GPG-KEY-remi && \
    rpm -Uvh http://rpms.remirepo.net/enterprise/remi-release-7.rpm && \
    yum-config-manager --enable remi-php72 && \
    yum -y install less wget httpd httpd-tools php php-gd php-ldap \
                   nfs-utils python-pip cronie rsyslog && \
    yum clean all && \
    pip install --upgrade pip && \
    pip install --upgrade awscli

# Fix up rsyslog and cron to run in a container
RUN \
    sed -i -e 's/^\$ModLoad imjournal/\#\$ModLoad imjournal/' \
           -e 's/^\#\$ModLoad imklog/\$ModLoad imklog/' \
           -e 's/^\#\$ModLoad immark/\$ModLoad immark/' \
           -e 's/^\$OmitLocalLogging on/\$OmitLocalLogging off/' \
           -e 's/^\$IMJournalStateFile imjournal.state/\#\$IMJournalStateFile imjournal.state/' \
           /etc/rsyslog.conf && \
    sed -i 's/^\$SystemLogSocketName/#\$SystemLogSocketName/' /etc/rsyslog.d/listen.conf && \
    sed -i '/session.*required.*pam_loginuid.so/d' /etc/pam.d/crond && \
    touch /var/log/cron

# Download & check & deploy dokuwiki & cleanup
RUN wget -q -O /dokuwiki.tgz "http://download.dokuwiki.org/src/dokuwiki/dokuwiki-$DOKUWIKI_VERSION.tgz" && \
    if [ "$DOKUWIKI_CSUM" != "$(md5sum /dokuwiki.tgz | awk '{print($1)}')" ];then echo "Wrong md5sum of downloaded file!"; exit 1; fi && \
    mkdir /dokuwiki && \
    tar -zxf dokuwiki.tgz -C /dokuwiki --strip-components 1 && \
    rm dokuwiki.tgz

# Add links to docuwiki
RUN \
    sed -i '/<Directory "\/var\/www\/html">/,/<\/Directory>/{s/AllowOverride None/AllowOverride All/g}' /etc/httpd/conf/httpd.conf && \
    rm -fr /var/www/html && \
    ln -s /dokuwiki /var/www/html && \
    cp /dokuwiki/.htaccess.dist /dokuwiki/.htaccess

# Set up ownership
RUN chown -R apache:apache /dokuwiki

# Configure apache
ADD dokuwiki-httpd.conf /etc/httpd/vhosts.d/docs.conf
ADD vhosts.conf /etc/httpd/conf.d/vhosts.conf
ADD apachelogging.sh /root/apachelogging.sh
RUN chmod a+x /root/apachelogging.sh
RUN /root/apachelogging.sh

# Add entrypoint script
ADD startup.sh /root/startup.sh
ADD update.sh /root/update.sh
RUN chmod a+x /root/startup.sh /root/update.sh

# Add backup script
ADD dokuwiki.backup.cron /etc/cron.d/dokuwiki.backup
RUN chmod 644 /etc/cron.d/dokuwiki.backup

EXPOSE 80
VOLUME ["/dokuwiki/data/","/dokuwiki/lib/plugins/","/dokuwiki/conf/","/dokuwiki/lib/tpl/","/var/log/"]

#ENTRYPOINT ["/usr/sbin/lighttpd", "-D", "-f", "/etc/lighttpd/lighttpd.conf"]
#ENTRYPOINT ["/usr/sbin/apachectl", "-DFOREGROUND"]
ENTRYPOINT ["/root/startup.sh"]
