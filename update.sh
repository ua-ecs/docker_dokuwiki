#!/usr/bin/env bash

# Modified from https://bitbucket.org/mprasil/docker_dokuwiki/src/master/docker-startup.sh

set -eu
set -o pipefail

# Updating DokuWiki may need to change files in directories which we hold in
# volumes, e.g. `data` or `conf`. Therefore, we need to make sure these files
# are there when the container is started, not only when it is created. We do
# this by keeping track which version we last installed and update as necessary,
# or fully populate these folders if this is the first run.

dokudir=/nfsdokuwiki
srcdir=/dokuwiki
verfile=.last-version
containerver="$(date -f <(echo "$DOKUWIKI_VERSION" | tr -d '[:alpha:]') +%s)"

if [ ! -d "$dokudir" ]; then
    echo "DokuWiki does not appear to be installed correctly at: $dokudir." >&2
    exit 1
fi

# Check for downgrade/overwrite parameters
if [ "$1" = 'downgrade' ]; then downgrade=1; else downgrade=0; fi
if [ "$1" = 'overwrite' ]; then overwrite=1; else overwrite=0; fi

if [ "$1" = 'run' ] || [ "$1" = 'downgrade' ] || [ "$1" = 'overwrite' ]; then
    # Check each volume directory in turn
    for d in lib/plugins lib/tpl; do
        if [ -f "$dokudir/$d/$verfile" ]; then
            volumever="$(date -f <(awk '{print $1}' "$dokudir/$d/$verfile" | tr -d '[:alpha:]') +%s)"
        else
            volumever=0
        fi

        if [ "$volumever" -eq "$containerver" ] && [ ! "$overwrite" -eq 1 ]; then
            # Do nothing for equal versions
            continue
        elif [ "$volumever" -lt "$containerver" ] || [ "$downgrade" -eq 1 ] || [ "$overwrite" -eq 1 ]; then
            # Current version already unpacked in the container,
            # and volumes are not bind-mapped yet

            # Then, update if the container version is newer than the volume version
            # Or if overridden using `downgrade`
            echo "Migrating $d..."
            cp -r "$srcdir/$d/"* "$dokudir/$d/"
            echo "$DOKUWIKI_VERSION" > "$dokudir/$d/$verfile"
        elif [ "$volumever" -gt "$containerver" ]; then
            # Otherwise print an error message and stop
            cat >&2 <<EOM
This volume has perviously been used with a newer version of DokuWiki.
If you want to force a downgrade (at your own risk!), run the \`downgrade\` command:
    docker run ... dokuwiki_uits donwgrade
EOM
        exit 2
        fi
    done
fi

exit 0
